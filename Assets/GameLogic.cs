﻿using UnityEngine;
using System.Collections;

public enum Turn {
	Player,
	Opponent
};

public class GameLogic : MonoBehaviour {
	
	public int OpponentHP;
	public int OpponentAP;
	
	public int PlayerHP;
	public int PlayerAP;
	
	private Turn turn;
	
	public bool IsPlayerTurn {
		get { return turn == Turn.Player; }
	}
	
	public bool IsGameOver {
		get { return IsPlayerDead || IsOpponentDead; }
	}
	
	public bool IsPlayerDead {
		get { return PlayerHP <= 0; }
	}
	
	public bool IsOpponentDead {
		get { return OpponentHP <= 0; }
	}
	
	public bool IsDraw {
		get { return IsPlayerDead && IsOpponentDead; }
	}
	
	void Start () {
		OpponentHP = 10;
		OpponentAP = 2;
		
		PlayerHP = 10;
		PlayerAP = 3;
		
		turn = Turn.Player;
	}
	
	void Update () {
		if (turn == Turn.Opponent) {
			PlayerHP -= OpponentAP;
			
			if (PlayerHP <= 0) {
				PlayerHP = 0;
			}
			
			turn = Turn.Player;
			
			Debug.Log(string.Format("Opponent Turn Ended, HP is now player:{0} opponent:{1}", PlayerHP, OpponentHP)); 
		}
	}
	
	public void PlayerFinishedTurn() {
		if (IsGameOver) return;
		
		turn = Turn.Opponent;
		OpponentHP -= PlayerAP;
		
		if (OpponentHP <= 0) {
			OpponentHP = 0;
		}
	}
}
