﻿using UnityEngine;
using System.Collections;

public class PlayerCardHPText : MonoBehaviour {
	
	public GameLogic GameLogic;
	public TextMesh HPTextMesh;
	public TextMesh APTextMesh;

	void Start () {
		
	}	
	
	void Update () {
		HPTextMesh.text = string.Format("{0}", GameLogic.PlayerHP);
		APTextMesh.text = string.Format("{0}", GameLogic.PlayerAP);
	}
}
