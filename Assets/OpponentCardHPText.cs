﻿using UnityEngine;
using System.Collections;

public class OpponentCardHPText : MonoBehaviour {

	public GameLogic GameLogic;
	public TextMesh HPTextMesh;
	public TextMesh APTextMesh;

	void Start () {
		
	}	
	
	void Update () {
		HPTextMesh.text = string.Format("{0}", GameLogic.OpponentHP);
		APTextMesh.text = string.Format("{0}", GameLogic.OpponentAP);
	}
}
