﻿using UnityEngine;
using System.Collections;

public class GameUI : MonoBehaviour {
	
	public GameLogic gameLogic;
	
	void OnGUI() {
		if (gameLogic.IsPlayerTurn) {
			if (GUI.Button(new Rect(10, 10, 100, 50), "Finished Turn")) {
				Debug.Log("Player Turn Finished");	
				gameLogic.PlayerFinishedTurn();
			}
		}
		
		if (gameLogic.IsGameOver) {
			if (gameLogic.IsDraw) {
				GUI.Label(new Rect(10, 100, 100, 20), "Draw Game");	
			}
			else if (gameLogic.IsPlayerDead) {
				GUI.Label(new Rect(10, 100, 100, 20), "Opponent Won Game");	
			} 
			else if (gameLogic.IsOpponentDead) {
				GUI.Label(new Rect(10, 100, 100, 20), "Player Won Game");	
			}
		}
	}
	
	void Start () {
		
	}
	
	void Update () {
	
	}
}
